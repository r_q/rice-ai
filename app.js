// app.js
App({
  onLaunch() {
    // 获取状态栏高度
    const {
      statusBarHeight
    } = wx.getSystemInfoSync();

    // 得到右上角菜单的位置尺寸
    const menuButtonObject = wx.getMenuButtonBoundingClientRect();
    const {
      top,
      height
    } = menuButtonObject;

    // 计算导航栏的高度
    // 此高度基于右上角菜单在导航栏位置垂直居中计算得到
    const navBarHeight = height + (top - statusBarHeight) * 2;

    // 计算状态栏与导航栏的总高度
    const statusNavBarHeight = statusBarHeight + navBarHeight;
    this.globalData.statusNavBarHeight = statusNavBarHeight;
    this.globalData.navBarHeight = navBarHeight;
    this.globalData.statusBarHeight = statusBarHeight;
  },
  globalData: {
    statusNavBarHeight: 0,
    navBarHeight: 0,
    statusBarHeight: 0,
  }
})
const app = getApp();
Component({
  options: {
    multipleSlots: true,
  },
  properties: {
    title: {
      type: String,
      optionalTypes: [Number, null],
    },
    back: {
      type: Boolean,
      value: false,
    },
  },
  lifetimes: {
    attached() {
      let {
        statusNavBarHeight,
        navBarHeight,
        statusBarHeight,
      } = app.globalData;
      this.setData({
        statusNavBarHeight,
        navBarHeight,
        statusBarHeight,
      });
    },
  },
  /**
   * 组件的初始数据
   */
  data: {
    statusNavBarHeight: 0,
    navBarHeight: 0,
    statusBarHeight: 0,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    backTrigger() {
      let that = this;
      wx.navigateBack({
        delta: 1,
        fail() {
          that.triggerEvnet("back");
        },
      });
    },
  },
});
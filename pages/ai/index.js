// pages/ai/index.js
import socket from "../../utils/socket"
let ws = null
let tempContext = ""
let tim = null

Page({
  data: {
    keybottom: 0,
    message: "",
    tabBarHeight: 0,
    end: 0,
    loading: false,
    current: false,
    scrollIntoView: "",
    // list: [{
    //   id: '1',
    //   content: "你好啊",
    //   role: "user"
    // }, {
    //   id: '2',
    //   content: "你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊",
    //   role: "sys"
    // }, {
    //   id: '3',
    //   content: "你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊",
    //   role: "sys"
    // }, {
    //   id: '4',
    //   content: "你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊",
    //   role: "sys"
    // }],
    list: []
  },
  handleSend() {
    if(this.data.message.trim().length<1) {
      wx.showToast({
        title: "请输入内容",
        icon: "none",
        mask: true,
        duration: 1200
      })
      return
    }
    if (this.data.loading) {
      wx.showToast({
        title: "请等待上一条信息返回",
        icon: "none",
        mask: true,
        duration: 1200
      })
      return
    }
    let list = [...this.data.list, {
      role: "user",
      content: this.data.message
    }]
    let socketMsg = {
      messageCallback: this.handleMessage,
      data: list
    }
    ws = new socket(socketMsg)
    let random = Math.random().toString(36).substr(2)
    this.setData({
      list: [...this.data.list, {
        id: random,
        role: "user",
        content: this.data.message
      }],
      message: "",
      loading: true,
      scrollIntoView: `it${this.data.list.length-1}`
    })
  },
  handleMessage({
    data
  }) {
    let temp = JSON.parse(data)
    let random = Math.random().toString(36).substr(2)
    if (!this.data.current) {
      this.setData({
        list: [...this.data.list, {
          id: random,
          role: "assistant",
          content: ""
        }],
        current: true
      })
    }
    let contents = `list[${this.data.list.length - 1}].content`
    let tempContents = this.data.list[this.data.list.length - 1].content
    console.log(contents)
    temp.payload.choices.text.forEach(element => {
      // tempContext += element.content
      this.setData({
        [contents]: tempContents += element.content,
        scrollIntoView: `it${this.data.list.length-1}`
      })
    });
    if (temp.header.code === 0) {
      if (temp && temp.header.status === 2) {
        this.setData({
          loading: false,
          current: false,
          scrollIntoView: `it${this.data.list.length-1}`
        })
        tempContext = ""
        ws.close()
        ws = null
      }
    }
  },
  getMessageListBox() {
    const query = wx.createSelectorQuery()
    query.selectAll('.end').boundingClientRect(res => {
      console.log(res)
    }).exec()
  },
  getLineHeight() {
    const query = wx.createSelectorQuery()
    query.selectAll('.message').fields({
      computedStyle: ["height", 'lineHeight']
    }, function (res) {
      let messageList = res.map(item => {
        item.height = item.height.replace('px', '') - 10
        item.lineHeight = item.lineHeight.replace('px', '') - 0
        return item
      })
      console.log(messageList)
    }).exec()
  },
  handleInput(e) {
    this.setData({
      message: e.detail.value
    })
  },
  onShow() {
    this.getMessageListBox()
  },
  getHeight() {
    return this.data.keybottom == 0 ? 'calc(100% - 100rpx)' : `calc(100% - 100rpx - ${this.data.keybottom})`
  },

  handleBlur(e) {
    console.log(e)
    this.setData({
      keybottom: 0
    })
  },
  handleKeyChange(e) {
    let h = e.detail.height
    if (tim) clearTimeout(tim)
    tim = setTimeout(() => {
      let systemInfo = wx.getSystemInfoSync()
      // 状态栏的高度
      let rpxStatusHeight = systemInfo.statusBarHeight
      // 导航栏的高度
      let rpxNavHeight = 44
      // 内容区的高度
      let rpxWindowHeight = systemInfo.windowHeight
      // 屏幕的高度
      let rpxScreentHeight = systemInfo.screenHeight
      // 底部tabBar的高度
      let tabBarHeight = rpxScreentHeight - rpxStatusHeight - rpxNavHeight - rpxWindowHeight
      let keyH = h - tabBarHeight
      this.setData({
        keybottom: keyH > -1 ? keyH : 0,
        tabBarHeight,
      })
      this.setData({
        end: this.data.end + 9999
      })
    })
  }
})
import socket from "../../utils/socket"
let app = getApp()
let ws = null
Page({
  data: {
    statusNavBarHeight: 0,
    navBarHeight: 0,
    statusBarHeight: 0,
    active: null,
    once: true,
    list: [],
    keybottom: 0,
    message: "",
    tabBarHeight: 0,
    end: 0,
    loading: false,
    current: false,
    endScroll: 0,
    scrollHeight: 0,
    scrollIntoView: "",
    // list: [{
    //   id: '1',
    //   content: "你好啊",
    //   role: "user"
    // }, {
    //   id: '2',
    //   content: "你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊",
    //   role: "sys"
    // }, {
    //   id: '3',
    //   content: "你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊",
    //   role: "sys"
    // }, {
    //   id: '4',
    //   content: "你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊你好啊",
    //   role: "sys"
    // }],
  },
  onLoad(options) {

  },
  onReady() {
    this.getHeights().then(r => {
      this.setData({
        endScroll: r
      })
    })
  },
  handleChange() {
    this.setData({
      active: !this.data.active,
      once: false
    })
  },
  onShow() {
    let {
      statusNavBarHeight,
      navBarHeight,
      statusBarHeight,
    } = app.globalData;
    this.setData({
      statusNavBarHeight,
      navBarHeight,
      statusBarHeight,
    });
  },
  onHide() {
    this.setData({
      active: false,
    })
  },
  onUnload() {},
  handleSetActive() {
    if (this.data.active) {
      this.setData({
        active: false
      })
    }
  },
  handleSend() {
    if (this.data.message.trim().length < 1) {
      wx.showToast({
        title: "请输入内容",
        icon: "none",
        mask: true,
        duration: 1200
      })
      return
    }
    if (this.data.loading) {
      wx.showToast({
        title: "请等待上一条信息返回",
        icon: "none",
        mask: true,
        duration: 1200
      })
      return
    }
    let list = [...this.data.list, {
      role: "user",
      content: this.data.message
    }]
    let socketMsg = {
      messageCallback: this.handleMessage,
      data: list
    }
    ws = new socket(socketMsg)
    let random = Math.random().toString(36).substr(2)
    this.setData({
      list: [...this.data.list, {
        id: random,
        role: "user",
        content: this.data.message
      }],
      message: "",
      loading: true,
      // scrollIntoView: `it${this.data.list.length-1}`
    })
    this.getHeights().then(r => {
      setTimeout(() => {
        this.setData({
          endScroll: r
        })
      })
    })
  },
  handleMessage({
    data
  }) {
    let temp = JSON.parse(data)
    let random = Math.random().toString(36).substr(2)
    if (!this.data.current) {
      this.setData({
        list: [...this.data.list, {
          id: random,
          role: "assistant",
          content: ""
        }],
        current: true
      })
    }
    let contents = `list[${this.data.list.length - 1}].content`
    let tempContents = this.data.list[this.data.list.length - 1].content
    temp.payload.choices.text.forEach(element => {
      this.setData({
        [contents]: tempContents += element.content,
      })
      this.getHeights().then(r => {
        this.setData({
          endScroll: r
        })
      })
    })
    if (temp.header.code === 0) {
      if (temp && temp.header.status === 2) {
        this.setData({
          loading: false,
          current: false,
          // scrollIntoView: `it${this.data.list.length-1}`
        })
        this.getHeights().then(r => {
          this.setData({
            endScroll: r
          })
        })
        ws.close()
        ws = null
      }
    }
  },
  handleInput(e) {
    this.setData({
      message: e.detail.value
    })
  },
  handleBlur(e) {
    this.setData({
      keybottom: 0,
      scrollHeight: 0
    })
  },
  handleKeyChange(e) {
    let h = e.detail.height || 0
    let scrollHeight = h + this.data.statusNavBarHeight
    this.setData({
      keybottom: h,
      scrollHeight
    })
    this.getHeights().then(r => {
      this.setData({
        endScroll: r
      })
    })
  },
  getHeights() {
    let query = wx.createSelectorQuery()
    return new Promise(reslove => {
      query.select('.scrollbox').fields({
        dataset: true,
        size: true,
        scrollOffset: true,
        properties: ['scrollX', 'scrollY'],
        computedStyle: ['margin', 'backgroundColor'],
        context: true,
      }, res => {
        reslove(res.scrollHeight)
      }).exec()
    })
  },
})